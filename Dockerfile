FROM openjdk:11

EXPOSE 8080

RUN mkdir -p /opt/application/jar
WORKDIR /opt/application/jar
COPY build/libs/vote-service.jar /opt/application/jar/vote-service.jar

ENTRYPOINT [ "java", \ 
    "-jar", \ 
    "/opt/application/jar/vote-service.jar" ]
