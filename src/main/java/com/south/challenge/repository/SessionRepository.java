package com.south.challenge.repository;

import java.util.List;
import java.util.Optional;

import com.south.challenge.entities.Session;
import com.south.challenge.entities.TopicVoting;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Long> {

    Optional<Session> findByTopicVoting(TopicVoting topicVoting);

    Optional<List<Session>> findByProduceMessage(Boolean produceMessage);

    Optional<List<Session>> findByOpenSession(Boolean openSession);

}
