package com.south.challenge.repository;

import java.util.List;
import java.util.Optional;

import com.south.challenge.entities.TopicVoting;
import com.south.challenge.entities.Vote;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VoteRepository extends JpaRepository<Vote, Long>{

    Optional<List<Vote>> findByTopicVoting(TopicVoting topic);

    Optional<Vote> findByDocumentAndTopicVoting(String document, TopicVoting topic);
    
}
