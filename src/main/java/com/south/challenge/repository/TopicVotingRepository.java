package com.south.challenge.repository;

import com.south.challenge.entities.TopicVoting;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicVotingRepository extends JpaRepository<TopicVoting, Long> {
    
}
