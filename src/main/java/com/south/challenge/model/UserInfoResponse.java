package com.south.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoResponse {
    private static final String ABLE_TO_VOTE = "ABLE_TO_VOTE";

    private String status;

    public Boolean isAbleToVote () {
        return getStatus().equalsIgnoreCase(ABLE_TO_VOTE);
    }
    
}
