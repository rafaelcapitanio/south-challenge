package com.south.challenge.model.result;

import com.south.challenge.model.session.SessionResponse;
import com.south.challenge.model.topic.TopicResponse;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultResponse {

    private SessionResponse session;

    private TopicResponse topic;

    private Long countYes;

    private Long countNo;

    private Long totalVotes;

}
