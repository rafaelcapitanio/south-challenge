package com.south.challenge.scheduler;

import com.south.challenge.qeuee.VProducer;
import com.south.challenge.service.SessionService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SendMenssageScheduleJob {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SessionService sessionService;

    @Autowired
    private VProducer vProducer;

    @Scheduled(cron = "${cron.send-message-time}")
    private void verifyAndSendAllClosedSessions() {
        logger.info("Scheduler is running now");

        sessionService.processClosedSessions().stream().forEach(json -> {
            logger.info("Producing message");
            vProducer.produceMessage(json);
        });

        logger.info("All messages has been produced");
    }
}
