package com.south.challenge.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.south.challenge.entities.Session;
import com.south.challenge.entities.TopicVoting;
import com.south.challenge.exception.Messages;
import com.south.challenge.exception.NotFoundException;
import com.south.challenge.model.session.SessionRequest;
import com.south.challenge.repository.SessionRepository;
import com.south.challenge.repository.TopicVotingRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SessionService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private VoteService voteService;

    @Autowired
    private TopicVotingRepository topicRepository;

    public Session openSession(Long topicVotingId, SessionRequest sessionRequest) {
        Optional<TopicVoting> topicEntity = topicRepository.findById(topicVotingId);

        if (topicEntity.isPresent()) {
            var sessionEntity = new Session(topicEntity.get(), LocalDateTime.now(), sessionRequest.getFinalVoting(),
                    false, true);
            return saveSession(sessionEntity);
        }

        logger.error(Messages.TOPIC_NOT_EXISTS);
        throw new IllegalArgumentException(Messages.TOPIC_NOT_EXISTS);
    }

    public Boolean isSessionOpen(TopicVoting topicVoting) {
        Optional<Session> optionalSession = sessionRepository.findByTopicVoting(topicVoting);
        if (optionalSession.isPresent()) {
            return LocalDateTime.now().isBefore(optionalSession.get().getFinalVoting());
        }

        logger.error(Messages.SESSION_NOT_EXISTS);
        throw new IllegalArgumentException(Messages.SESSION_NOT_EXISTS);
    }

    public List<String> processClosedSessions() {
        Optional<List<Session>> allOpenSessions = sessionRepository.findByOpenSession(true);

        if (allOpenSessions.isPresent()) {

            return allOpenSessions.get().stream()
                .filter(session -> session.getFinalVoting().isBefore(LocalDateTime.now()) && session.getProduceMessage() == Boolean.FALSE)
                .map(session -> updateAttrSession(session))
                .map(session -> voteService.buildMessage(session))
                .collect(Collectors.toList());
        }

        logger.error(Messages.ANY_CLOSED_SESSION_TO_PROCCESS);
        throw new NotFoundException(Messages.ANY_CLOSED_SESSION_TO_PROCCESS);
    }

    public Session updateAttrSession(Session session) {

        session.setProduceMessage(Boolean.TRUE);
        session.setOpenSession(Boolean.FALSE);

        return saveSession(session);
    }

    public Session saveSession(Session session) {

        return sessionRepository.save(session);
    }

    public Optional<Session> getSessionByTopic (TopicVoting topic) {
        return sessionRepository.findByTopicVoting(topic);
    }
}
