package com.south.challenge.service;

import com.south.challenge.entities.TopicVoting;
import com.south.challenge.exception.Messages;
import com.south.challenge.exception.UnavailableServiceException;
import com.south.challenge.model.topic.TopicRequest;
import com.south.challenge.repository.TopicVotingRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TopicVotingRepository topicRepository;

    public TopicVoting createTopic(TopicRequest tRequest) {
        try {
            logger.debug("create topic: {0}", tRequest);
            var topicEntity = new TopicVoting(tRequest.getDescription());
            logger.debug("saving Topic");
    
            return topicRepository.save(topicEntity);
        } catch (Exception e) {
            logger.error("error to create topic. Error message: {0}", e.getMessage());
        }

        throw new UnavailableServiceException(Messages.SERVICE_UNAVAILABLE);
    }

}
