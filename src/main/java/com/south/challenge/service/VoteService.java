package com.south.challenge.service;

import java.util.List;
import java.util.Optional;

import com.google.gson.Gson;
import com.south.challenge.client.UserInfoClient;
import com.south.challenge.entities.Session;
import com.south.challenge.entities.TopicVoting;
import com.south.challenge.entities.Vote;
import com.south.challenge.exception.ClosedSessionException;
import com.south.challenge.exception.Messages;
import com.south.challenge.exception.NotFoundException;
import com.south.challenge.exception.UnableToVoteException;
import com.south.challenge.exception.UnavailableToComputeVotesException;
import com.south.challenge.model.UserInfoResponse;
import com.south.challenge.model.result.ResultResponse;
import com.south.challenge.model.session.SessionResponse;
import com.south.challenge.model.topic.TopicResponse;
import com.south.challenge.model.vote.VoteRequest;
import com.south.challenge.repository.TopicVotingRepository;
import com.south.challenge.repository.VoteRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class VoteService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserInfoClient userInfoClient;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private TopicVotingRepository topicRepository;

    @Autowired
    private SessionService sessionService;

    public Vote vote(Long topicVotingId, VoteRequest voteRequest) {

        try {
            ResponseEntity<UserInfoResponse> documentResponse = userInfoClient.abbleToVote(voteRequest.getDocument());

            if (documentResponse.getBody().isAbleToVote().equals(Boolean.TRUE)) {

                Optional<TopicVoting> topic = getTopicById(topicVotingId);
                if (topic.isPresent() && sessionService.isSessionOpen(topic.get()).equals(Boolean.TRUE)
                        && !verifyDocumentAlreadyVote(voteRequest.getDocument(), topic.get())) {

                    var voteEntity = new Vote(topic.get(), voteRequest.getDocument(), voteRequest.getVote());
                    return voteRepository.save(voteEntity);
                }
                logger.error(Messages.TOPIC_NOT_EXISTS);
                throw new NotFoundException(Messages.TOPIC_NOT_EXISTS);
            }

            logger.error(Messages.UNABLE_TO_VOTE);
            throw new UnableToVoteException(Messages.UNABLE_TO_VOTE);
        } catch (HttpClientErrorException e) {
            logger.error(e.getMessage());
            throw new NotFoundException(Messages.DOCUMENT_NOT_FOUND);
        }
    }

    public String getResult(Long topicVotingId) {
        Optional<TopicVoting> topic = topicRepository.findById(topicVotingId);

        if (topic.isPresent() && sessionService.isSessionOpen(topic.get()).equals(Boolean.FALSE)) {

            Optional<Session> session = sessionService.getSessionByTopic(topic.get());
            if (session.isPresent()) {
                return session.get().getProducedMessage();
            }

            logger.error(Messages.SESSION_IS_NOT_CLOSE);
            throw new ClosedSessionException(Messages.SESSION_IS_NOT_CLOSE);
        }

        logger.error(Messages.TOPIC_NOT_EXISTS);
        throw new IllegalArgumentException(Messages.TOPIC_NOT_EXISTS);
    }

    public String buildMessage(Session session) {
        try {
            var resultResponse = new ResultResponse();
            resultResponse.setSession(new SessionResponse(session.getStartVoting(), session.getFinalVoting()));
            resultResponse.setTopic(new TopicResponse(session.getTopicVoting().getDescription()));
    
            Optional<List<Vote>> allVotesByTopic = voteRepository.findByTopicVoting(session.getTopicVoting());
    
            if (allVotesByTopic.isPresent()) {
                long countYes = allVotesByTopic.get().stream().filter(vote -> vote.getVoteValue().equals(Boolean.TRUE))
                        .count();
                long countNo = allVotesByTopic.get().stream().filter(vote -> vote.getVoteValue().equals(Boolean.FALSE))
                        .count();
                long totalVotes = allVotesByTopic.get().stream().count();
    
                resultResponse.setCountNo(countNo);
                resultResponse.setCountYes(countYes);
                resultResponse.setTotalVotes(totalVotes);
            }
    
            String json = new Gson().toJson(resultResponse);
            session.setProducedMessage(json);
            sessionService.saveSession(session);
    
            return json;
        } catch (Exception e){
            logger.error(e.getMessage());
        }

        throw new UnavailableToComputeVotesException(Messages.SERVICE_UNAVAILABLE);
    }

    @Cacheable(cacheNames = "topicVoting", key = "#topicVotingId")
    private Optional<TopicVoting> getTopicById(Long topicVotingId) {
        return topicRepository.findById(topicVotingId);
    }

    private Boolean verifyDocumentAlreadyVote(String document, TopicVoting topic) {
        return voteRepository.findByDocumentAndTopicVoting(document, topic).isPresent();
    }

}
