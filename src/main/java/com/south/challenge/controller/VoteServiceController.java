package com.south.challenge.controller;

import com.south.challenge.entities.Session;
import com.south.challenge.entities.TopicVoting;
import com.south.challenge.entities.Vote;
import com.south.challenge.model.session.SessionRequest;
import com.south.challenge.model.topic.TopicRequest;
import com.south.challenge.model.vote.VoteRequest;
import com.south.challenge.service.SessionService;
import com.south.challenge.service.TopicService;
import com.south.challenge.service.VoteService;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/v1")
public class VoteServiceController {
    
    @Autowired
    private SessionService sessionService;

    @Autowired
    private VoteService voteService;

    @Autowired
    private TopicService topicService;

    @PostMapping("/topics-voting")
    public ResponseEntity<TopicVoting> createTopic(@RequestBody TopicRequest tRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(topicService.createTopic(tRequest));
    }
    
    @PostMapping("/topics-voting/{topicVotingId}/vote")
    public ResponseEntity<Vote> vote(@PathVariable Long topicVotingId, @RequestBody @NotNull VoteRequest vRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(voteService.vote(topicVotingId, vRequest));
    }

    @PostMapping("/topics-voting/{topicVotingId}/open-session")
    public ResponseEntity<Session> openSession(@PathVariable Long topicVotingId, @RequestBody @NotNull SessionRequest sessionRequest) {
        return ResponseEntity.status(HttpStatus.OK).body(sessionService.openSession(topicVotingId, sessionRequest));
    }

    @GetMapping("/topics-voting/{topicVotingId}/result")
    public ResponseEntity<String> getAll(@PathVariable Long topicVotingId) {
        return ResponseEntity.status(HttpStatus.OK).body(voteService.getResult(topicVotingId));
    }
}
