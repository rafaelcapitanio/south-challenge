package com.south.challenge.client;

import com.south.challenge.model.UserInfoResponse;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "UserInfoClient", url = "${clients.user-info.url}")
public interface UserInfoClient {

    @GetMapping(path = "/{document}")
    ResponseEntity<UserInfoResponse> abbleToVote(@PathVariable("document") String document);

}
