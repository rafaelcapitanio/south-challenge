package com.south.challenge.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "session")
public class Session {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long sessionId;

	@OneToOne()
	@JoinColumn(name = "topic_id")
	private TopicVoting topicVoting;

	@Column(name = "startVoting", nullable = false)
	private LocalDateTime startVoting;

	@Column(name = "finalVoting", nullable = false)
	private LocalDateTime finalVoting;

	@Column(name = "produce_message")
	private Boolean produceMessage;

	@Column(name = "produced_message")
	@Lob
	private String producedMessage;

	@Column(name = "open_session")
	private Boolean openSession;

	public Session(TopicVoting topicVoting, LocalDateTime startVoting, LocalDateTime finalVoting, Boolean produceMessage, Boolean openSession) {
		this.topicVoting = topicVoting;
		this.startVoting = startVoting;
		this.finalVoting = finalVoting;
		this.produceMessage = produceMessage;
		this.openSession = openSession;
	}
}
