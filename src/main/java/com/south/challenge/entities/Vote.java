package com.south.challenge.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "vote")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Vote {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long voteId;

	@OneToOne()
	@JoinColumn(name = "topic_id")
	private TopicVoting topicVoting;

	@Column(name = "document")
	private String document;

	@Column(name = "vote")
	private Boolean voteValue;

	public Vote(TopicVoting topicVoting, String document, Boolean voteValue) {
		this.topicVoting = topicVoting;
		this.document = document;
		this.voteValue = voteValue;
	}

}
