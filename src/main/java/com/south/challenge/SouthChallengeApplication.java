package com.south.challenge;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableRabbit
@EnableFeignClients
@EnableScheduling
@EnableSwagger2
@EnableAutoConfiguration
public class SouthChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SouthChallengeApplication.class, args);
	}

}
