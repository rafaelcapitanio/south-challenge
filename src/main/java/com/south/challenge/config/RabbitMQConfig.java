package com.south.challenge.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Configuration
@Data
public class RabbitMQConfig {
    
    @Value("${rabbitmq.exchange}")
	private String exchange;
	
	@Value("${rabbitmq.routing-key}")
	private String routingKey;

	@Value("${rabbitmq.queue-name}")
	private String queueName;


    @Bean
    public Queue myQueue() {
        return new Queue(queueName, true);
    }

}
