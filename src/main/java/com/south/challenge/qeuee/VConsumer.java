package com.south.challenge.qeuee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class VConsumer {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

	@RabbitListener(queues = {"${rabbitmq.queue-name}"})
    public void receive(@Payload String fileBody) {
        logger.info("Consumer: {0}", fileBody);
    }
	
}
