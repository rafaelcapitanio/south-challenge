package com.south.challenge.exception;

import org.springframework.web.client.RestClientException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RestClientException {

	public NotFoundException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = -1982323927440862743L;

}
