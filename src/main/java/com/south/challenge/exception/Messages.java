package com.south.challenge.exception;

public class Messages {

	public static final String SESSION_IS_CLOSED = "The session is closed.";
	public static final String TOPIC_NOT_EXISTS = "The topic not exist.";
	public static final String SESSION_NOT_EXISTS = "The session not exist.";
	public static final String SESSION_IS_NOT_CLOSE = "The topic voting session is not close.";
	public static final String DOCUMENT_NOT_FOUND = "The document not exists.";
	public static final String UNABLE_TO_VOTE = "Document invalid to vote.";
	public static final String ANY_CLOSED_SESSION_TO_PROCCESS = "Any closed session to proccess";
	public static final String RESULT_NOT_FOUND = "Result not found to this topic voting";
	public static final String SERVICE_UNAVAILABLE = "Service Unavaible";
	
}
