package com.south.challenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
public class UnavailableToComputeVotesException extends RuntimeException {

	private static final long serialVersionUID = -6447595330890225209L;

	public UnavailableToComputeVotesException(String message) {
		super(message);
	}
	
}
