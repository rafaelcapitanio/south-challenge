package com.south.challenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UnableToVoteException extends RuntimeException {

	private static final long serialVersionUID = -6447595330890225209L;

	public UnableToVoteException(String message) {
		super(message);
	}
	
}
