package com.south.challenge.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import com.south.challenge.exception.Messages;
import com.south.challenge.exception.UnavailableServiceException;
import com.south.challenge.model.topic.TopicRequest;
import com.south.challenge.repository.TopicVotingRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

@RunWith(MockitoJUnitRunner.class)
public class TopicServiceTest {

    @InjectMocks
    private TopicService topicService;

    @Mock
    private TopicVotingRepository topicRepository;

    @Mock
    private Logger logger;

    @Mock
    private Messages message;

    @Test
    public void test_create_topic_success () {

        TopicRequest tRequest = new TopicRequest("Test Junit");

        topicService.createTopic(tRequest);

        verify(logger, never()).error(anyString());
    }

    @Test(expected = UnavailableServiceException.class)
    public void test_create_topic_error () {
        topicService.createTopic(null);
    }
}
