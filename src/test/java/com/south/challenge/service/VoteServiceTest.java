package com.south.challenge.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.south.challenge.client.UserInfoClient;
import com.south.challenge.entities.Session;
import com.south.challenge.entities.TopicVoting;
import com.south.challenge.entities.Vote;
import com.south.challenge.exception.ClosedSessionException;
import com.south.challenge.exception.Messages;
import com.south.challenge.exception.NotFoundException;
import com.south.challenge.exception.UnableToVoteException;
import com.south.challenge.exception.UnavailableToComputeVotesException;
import com.south.challenge.model.UserInfoResponse;
import com.south.challenge.model.vote.VoteRequest;
import com.south.challenge.repository.TopicVotingRepository;
import com.south.challenge.repository.VoteRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RunWith(MockitoJUnitRunner.class)
public class VoteServiceTest {

    @InjectMocks
    private VoteService voteService;

    @Mock
    private UserInfoClient userInfoClient;

    @Mock
    private VoteRepository voteRepository;

    @Mock
    private TopicVotingRepository topicRepository;

    @Mock
    private SessionService sessionService;

    @Mock
    private Logger logger;

    @Mock
    private Messages message;

    @Test
    public void test_create_vote_success() {
        Long topicVotingId = 1L;
        VoteRequest vRequest = new VoteRequest("87027719002", true);
        UserInfoResponse userInfoResponse = new UserInfoResponse("ABLE_TO_VOTE");
        ResponseEntity<UserInfoResponse> resp = new ResponseEntity<UserInfoResponse>(userInfoResponse, HttpStatus.ACCEPTED);
        Optional<TopicVoting> topic = Optional.of(new TopicVoting(1L, "Test Junit"));

        when(userInfoClient.abbleToVote(vRequest.getDocument())).thenReturn(resp);

        when(topicRepository.findById(topicVotingId)).thenReturn(topic);

        when(voteRepository.findByDocumentAndTopicVoting(vRequest.getDocument(), topic.get())).thenReturn(Optional.ofNullable(null));

        when(sessionService.isSessionOpen(topic.get())).thenReturn(Boolean.TRUE);

        voteService.vote(topicVotingId, vRequest);

        verify(logger, never()).error(anyString());
    }

    @Test(expected = NotFoundException.class)
    public void test_create_vote_error_not_found() {
        Long topicVotingId = 1L;
        VoteRequest vRequest = new VoteRequest("87027719002", true);
        UserInfoResponse userInfoResponse = new UserInfoResponse("ABLE_TO_VOTE");
        ResponseEntity<UserInfoResponse> resp = new ResponseEntity<UserInfoResponse>(userInfoResponse, HttpStatus.ACCEPTED);

        when(userInfoClient.abbleToVote(vRequest.getDocument())).thenReturn(resp);

        voteService.vote(topicVotingId, vRequest);
    }

    @Test(expected = UnableToVoteException.class)
    public void test_create_vote_error_unable() {
        Long topicVotingId = 1L;
        VoteRequest vRequest = new VoteRequest("87027719002", true);
        UserInfoResponse userInfoResponse = new UserInfoResponse("");
        ResponseEntity<UserInfoResponse> resp = new ResponseEntity<UserInfoResponse>(userInfoResponse, HttpStatus.ACCEPTED);

        when(userInfoClient.abbleToVote(vRequest.getDocument())).thenReturn(resp);

        voteService.vote(topicVotingId, vRequest);
    }

    @Test
    public void test_get_result_success() {
        Long topicVotingId = 1L;
        Optional<TopicVoting> topic = Optional.of(new TopicVoting(topicVotingId, "Test Junit"));
        Session session = new Session(topic.get(), LocalDateTime.now(), LocalDateTime.now(), false, true);

        when(topicRepository.findById(topicVotingId)).thenReturn(topic);
        when(sessionService.isSessionOpen(topic.get())).thenReturn(Boolean.FALSE);
        when(sessionService.getSessionByTopic(topic.get())).thenReturn(Optional.of(session));

        voteService.getResult(topicVotingId);

        verify(logger, never()).error(anyString());
    }

    @Test(expected = ClosedSessionException.class)
    public void test_get_result_error_closed_session() {
        Long topicVotingId = 1L;
        Optional<TopicVoting> topic = Optional.of(new TopicVoting(topicVotingId, "Test Junit"));

        when(topicRepository.findById(topicVotingId)).thenReturn(topic);
        when(sessionService.isSessionOpen(topic.get())).thenReturn(Boolean.FALSE);
        when(sessionService.getSessionByTopic(topic.get())).thenReturn(Optional.ofNullable(null));

        voteService.getResult(topicVotingId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_get_result_error_illegal() {
        Long topicVotingId = 1L;

        voteService.getResult(topicVotingId);
    }

    @Test
    public void test_build_message_success() {
        Optional<TopicVoting> topic = Optional.of(new TopicVoting(1L, "Test Junit"));
        Session session = new Session(1L, topic.get(), LocalDateTime.now(), LocalDateTime.now(), false, null, true);
        Optional<List<Vote>> votes = Optional.of(getVotes(topic.get()));

        when(voteRepository.findByTopicVoting(session.getTopicVoting())).thenReturn(votes);

        voteService.buildMessage(session);

        verify(logger, never()).error(anyString());
    }
    
    @Test
    public void test_build_message_no_votes() {
        Optional<TopicVoting> topic = Optional.of(new TopicVoting(1L, "Test Junit"));
        Session session = new Session(1L, topic.get(), LocalDateTime.now(), LocalDateTime.now(), false, null, true);

        when(voteRepository.findByTopicVoting(session.getTopicVoting())).thenReturn(Optional.ofNullable(null));

        voteService.buildMessage(session);

        verify(logger, never()).error(anyString());
    }

    @Test(expected = UnavailableToComputeVotesException.class)
    public void test_build_message_unavailable() {

        voteService.buildMessage(null);
    }

    private List<Vote> getVotes(TopicVoting topic) {
        List<Vote> votes = new ArrayList<Vote>();

        for(int i = 1; i <=3; i++) {
            Vote vote = new Vote(Long.valueOf(String.valueOf(i)), topic, "90710473001", true);
            votes.add(vote);
        }

        for(int i = 4; i <=7; i++) {
            Vote vote = new Vote(Long.valueOf(String.valueOf(i)), topic, "76712170001", false);
            votes.add(vote);
        }

        return votes;
    }

}
