package com.south.challenge.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.south.challenge.entities.Session;
import com.south.challenge.entities.TopicVoting;
import com.south.challenge.exception.Messages;
import com.south.challenge.model.session.SessionRequest;
import com.south.challenge.repository.SessionRepository;
import com.south.challenge.repository.TopicVotingRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

@RunWith(MockitoJUnitRunner.class)
public class SessionServiceTest {

    @InjectMocks
    private SessionService sessionService;

    @Mock
    private VoteService voteService;

    @Mock
    private TopicVotingRepository topicRepository;

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private Logger logger;

    @Mock
    private Messages message;

    @Test
    public void test_open_session_success () {
        Long topicVotingId = 1L;
        when(topicRepository.findById(topicVotingId)).thenReturn(Optional.of(new TopicVoting(topicVotingId, "Test Junit")));
        
        SessionRequest sessionRequest = new SessionRequest(LocalDateTime.now());

        sessionService.openSession(topicVotingId, sessionRequest);

        verify(logger, never()).error(anyString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_open_session_error () {
        Long topicVotingId = 1L;
        SessionRequest sessionRequest = new SessionRequest(LocalDateTime.now());

        sessionService.openSession(topicVotingId, sessionRequest);
    }

    @Test
    public void test_is_open_session_success () {
        TopicVoting topicVoting = new TopicVoting(1L, "Test Junit");
        Session session = new Session(topicVoting, LocalDateTime.now(), LocalDateTime.now(), false, true);

        when(sessionRepository.findByTopicVoting(topicVoting)).thenReturn(Optional.of(session));

        sessionService.isSessionOpen(topicVoting);

        verify(logger, never()).error(anyString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_is_open_session_error () {
        TopicVoting topicVoting = new TopicVoting(1L, "Test Junit");

        when(sessionRepository.findByTopicVoting(topicVoting)).thenReturn(Optional.ofNullable(null));

        sessionService.isSessionOpen(topicVoting);
    }

    @Test
    public void test_proccess_closed_sessions () {

        Optional<TopicVoting> topic = Optional.of(new TopicVoting(1L, "Test Junit"));
        Session session = new Session(1L, topic.get(), LocalDateTime.now(), LocalDateTime.now(), false, null, true);
        List<Session> sessions = new ArrayList<Session>();
        sessions.add(session);

        when(sessionRepository.findByOpenSession(true)).thenReturn(Optional.of(sessions));

        sessionService.processClosedSessions();

        verify(logger, never()).error(anyString());
    }

    @Test
    public void test_get_session_by_topic () {
        TopicVoting topic =new TopicVoting(1L, "Test Junit");
        Session session = new Session(1L, topic, LocalDateTime.now(), LocalDateTime.now(), false, null, true);

        when(sessionRepository.findByTopicVoting(topic)).thenReturn( Optional.of(session));

        sessionService.getSessionByTopic(topic);
    }

}
